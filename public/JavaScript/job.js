const JOB_IMAGE = document.querySelector(".information-container .image-container .image")
const JOB_JOB = document.querySelector(".information-container .text-container .job");
const JOB_COMPANY = document.querySelector(".information-container .text-container .company .company-link");
const JOB_LOCATION = document.querySelector(".information-container .text-container .location");
const JOB_TYPE = document.querySelector(".information-container .text-container .type");
const JOB_DESCRIPTION = document.querySelector(".description-container .description");

const buildJob = job => {
    JOBS_PAGE.classList.remove("active");
    JOB_PAGE.classList.add("active");

    JOB_IMAGE.src = job.company_logo;
    JOB_JOB.innerText = job.title;
    JOB_COMPANY.innerText = job.company;
    JOB_COMPANY.href = job.company_url;
    JOB_LOCATION.innerText = job.location;
    JOB_TYPE.innerText = job.type;
    JOB_DESCRIPTION.innerHTML = job.description;
}