/////////////////

const STEPS = document.querySelectorAll(".step");
const QUESTIONS = document.querySelectorAll(".question");
const INPUTS = document.querySelectorAll(".input");

const NEXT = document.querySelector(".button.next");
const SKIP = document.querySelector(".button.button-alternate");

const NUMBER = document.querySelector(".number-container");
const NUMBERCOUNTER = document.querySelector(".number-container #number");

const MAIN_PAGE = document.querySelector(".main");
const JOBS_PAGE = document.querySelector(".jobs");
const JOB_PAGE = document.querySelector(".looked-at-job");

const IMPRINT = document.querySelector(".imprint");
const MODAL = document.querySelector(".modal");

const CLOSE_BUTTON = document.querySelector(".close-container");

/////////////////

let current = 0; // Needed to modify the HTML

/////////////////

let data = ["", "", ""]; // Input to feed the API request
let response_json;
let totalJobs = 0;

/////////////////

let moneyArray = [];
let amountArray = [];
let percentageArray = [];

/////////////////

let counter = 0;

/////////////////

const OPTIONS = {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
};

/////////////////

const openJobs = () => {
    MAIN_PAGE.classList.remove("active");
    JOBS_PAGE.classList.add("active");
    buildJobs(response_json.length);
}

const modifyHTML = index => {
    current = index;

    STEPS.forEach(it => it.classList.remove("active"));
    QUESTIONS.forEach(it => it.classList.remove("active"));
    INPUTS.forEach(it => it.classList.remove("active"));

    STEPS[index].classList.add("active");
    QUESTIONS[index].classList.add("active");
    INPUTS[index].classList.add("active");
}

const changeNumbers = () => {
    NUMBERCOUNTER.classList.add("active");
    let jump = 1;
    if (Math.round(counter) == totalJobs) {
        NUMBERCOUNTER.classList.remove("active");
        return;
    }
    counter = counter > totalJobs ? counter - jump : counter + jump;

    NUMBERCOUNTER.innerText = Math.round(counter);
    setTimeout(changeNumbers, 1000 / 100);
}

const calculateSalary = data => {
    let max = 0;
    let total = 0;
    let median = 0;

    moneyArray = [];
    amountArray = [];
    percentageArray = [];
    Object.values(data.histogram).forEach (it => amountArray.push(parseInt(it)));
    
    for (let i = 0; i < amountArray.length; i++) {
        moneyArray.push((i+1)*10000);
        if (amountArray[i] > max) {
            max = amountArray[i];
            median = (i + 1) * 10000;
        };

        total += amountArray[i];
    }

    for (let i = 0; i < amountArray.length; i++) {
        percentageArray.push((amountArray[i] / total) * 100);
    }

    loadElement(moneyArray, percentageArray);

    let percentage = Math.round((max / total) * 100);

    document.querySelector(".statistic").innerText = `The median is ${median}€, with a ${percentage}%`;
}

const fetchAPI = () => {
    OPTIONS.body = JSON.stringify(data);
    fetch("/api", OPTIONS).then(response => {
        response.json().then(response => {
            response_json = response;

            openJobs(); // Switched from main-page to jobs-page
            getAverageSalary();
        });
    });
}

const getTotalJobs = () => {
    OPTIONS.body = JSON.stringify(data);
    fetch("/totaljobs", OPTIONS).then(response => {
        response.json().then(response => {
            totalJobs = response;

            changeNumbers();
        });
    });
}

const getAverageSalary = () => {
    fetch("/salary", OPTIONS).then(response => {
        response.json().then(response => {
            calculateSalary(response);
        })
    });
}

const next = () => {
    data[current] = INPUTS[current].value;
    if (current >= 2) return fetchAPI();

    current++;
    modifyHTML(current);
    getTotalJobs();
}

/////////////////

for (let i = 0; i < STEPS.length; i++) STEPS[i].addEventListener("click", () => modifyHTML(i));

for (let i = 0; i < INPUTS.length; i++) {
    INPUTS[i].addEventListener("input", () => data[current] = INPUTS[current].value);
}

NEXT.addEventListener("click", () => next());

SKIP.addEventListener("click", () => fetchAPI());

window.addEventListener("keyup", event => {
    if (event.key === "Enter") next();
});

IMPRINT.addEventListener("click", () => {
    MODAL.classList.toggle("active");

    window.addEventListener("keyup", event => {
        if (event.key === "Escape") MODAL.classList.remove("active");
    })
});

CLOSE_BUTTON.addEventListener("click", () => MODAL.classList.toggle("active"));
/////////////////

getTotalJobs();

/////////////////