const loadElement = (moneyArray, percentageArray) => {
    const CANVAS = document.querySelector("canvas");
    const ctx = CANVAS.getContext("2d");

    let yMultiplier = CANVAS.parentElement.clientHeight / 50; //CANVAS.parentElement.clientHeight;

    const updateCanvas = () => {
        CANVAS.width = CANVAS.parentElement.clientWidth;
        CANVAS.height = CANVAS.parentElement.clientHeight;

        yMultiplier = CANVAS.parentElement.clientHeight / 50;
    }
    updateCanvas();

    window.addEventListener("resize", updateCanvas);
    window.addEventListener("mousemove", event => {
        let rect = CANVAS.getBoundingClientRect();
        mouse.x = event.clientX - rect.left;
        mouse.y = event.clientY - rect.top;
    });

    /* CANVAS SETTINGS */

    let dataArray = new Array(5);
    let rects = [];

    let mouse = {
        x: 0,
        y: 0,
        radius: 5
    }

    class DataObject {
        constructor(height, dataIndex) {
            this.height = height;
            this.dataIndex = dataIndex;
            this.color = "#00ff44";
            this.position = 0;
            this.margin = 50;
        }

        draw() {
            let dBO = (CANVAS.width - this.margin) / rects.length;
            this.graph(dBO);
        }

        graph(dBO) {
            let x = (this.dataIndex * dBO) + this.margin;
            let y = (dataArray[this.dataIndex] * (-1 * yMultiplier)) +  (CANVAS.height - 5);
            
            // Graph 50%
            ctx.font = "16px Roboto";
            ctx.fillStyle = "#fff";
            ctx.textAlign = "left";
            ctx.fillText("50%", 0, 16);

            // Graph lines
            ctx.beginPath();
            ctx.rect(0 + this.margin, 0, 0, CANVAS.height);
            ctx.rect(0 + this.margin, CANVAS.height, CANVAS.width, 0);
            ctx.stroke();
            ctx.closePath();

            // dots & lines
            drawInformation(x, y, dBO, this.dataIndex, this.margin);

            // interactivity
            if (getDistance(mouse.x, mouse.y, x, y) < mouse.radius + 10) {
                drawLineInformation(mouse.x, mouse.y, this.dataIndex);
            }
        }
    }

    const drawInformation = (x, y, dBO, dataIndex, margin) => {
        // lines
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.moveTo(x, y);
        ctx.lineTo(((dataIndex + 1) * dBO) + margin, (dataArray[dataIndex + 1] * (-1 * yMultiplier)) +  (CANVAS.height - 5));
        ctx.strokeStyle = "#03DAC6";
        ctx.stroke();
        ctx.closePath();

        // dots
        ctx.beginPath();
        ctx.arc(x, y, 5, 0, 2 * Math.PI);
        ctx.fillStyle = "#03DAC6";
        ctx.fill();
        ctx.closePath();
    }

    const drawLineInformation = (x, y, dataIndex) => {
        ctx.beginPath();
        ctx.arc(x, y, 25, 0, 2 * Math.PI);
        ctx.fillStyle = "#03DAC6";
        ctx.fill();
        ctx.closePath();

        // text
        ctx.beginPath();
        ctx.font = "16px Arial";
        ctx.fillStyle = "#fff";
        ctx.textAlign = "center";
        ctx.fillStyle = "#000";
        ctx.fillText((moneyArray[dataIndex] / 1000) + "k", x, y + (12.5-8));
        ctx.closePath();
    }

    const getDistance = (x1, y1, x2, y2) => {
        let xDistance = x2 - x1;
        let yDistance = y2 - y1;

        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }

    const init = () => {
        dataArray = percentageArray;
        rects = [];
        for (let i = 0; i < dataArray.length; i++) rects.push(new DataObject(dataArray[i], i));
    }


    const animate = () => {
        requestAnimationFrame(animate);
        ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);

        rects.forEach(it => it.draw());
    }

    init();
    animate();
}

///