const express = require("express");
const fetch = require("node-fetch");
require("dotenv").config();

const app = express();

const PORT = process.env.PORT || 8080;

app.use(express.static("public"));
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}
Open a browser with the URL localhost:${PORT}`);
});
app.use(express.json({limit: "1mb"}));

//

let jobs;

// Responding with all the jobs based on the request
app.post("/api", async (request, response) => {
    const data = request.body;
    let job = data[0];
    let location = data[1];
    let fulltime = data[2];
    if (fulltime === "Yes" || fulltime === "Yes") fulltime = "true";
    console.log(fulltime);
    const API = `https://jobs.github.com/positions.json?description=${job}&location=${location}&full_time=${fulltime}`;
    let api_response = await fetch(API);
    let json = await api_response.json();
    jobs = json;
    response.json(json);
});

// Responding with the total amount of available jobs
app.post("/totaljobs", async (request, response) => {
    let totalJobs = 0;
    const data = request.body;
    let job = data[0];
    let location = data[1];
    let fulltime = data[2];
    if (fulltime === "Yes" || fulltime === "Yes") fulltime = "true";
    for (let pageNumber = 1; pageNumber <= 10; pageNumber++) {
        const API = `https://jobs.github.com/positions.json?description=${job}&location=${location}&full_time=${fulltime}&page=${pageNumber}`;
        let api_response = await fetch(API);
        let json = await api_response.json();
        if (json.length <= 0) break;
        totalJobs += json.length;
    }
    response.json(totalJobs);
});

app.post("/salary", async(request, response) => {
    let job = request.body[0];
    const app_id = process.env.API_ID;
    const app_key = process.env.API_KEY;
    const API = `http://api.adzuna.com/v1/api/jobs/gb/histogram?content-type=application/json&what=${job}&app_id=${app_id}&app_key=${app_key}`;
    let api_response = await fetch(API);
    let json = await api_response.json();
    response.json(json);
});