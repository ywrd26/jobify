// LEARN TO EXPORT AND IMPORT STUFF!!!!
const JOBS_CONTAINER = document.querySelector(".jobs-container");
let JOBS = [];

const jobListener = () => {
    for (let i = 0; i < JOBS.length; i++) {
        JOBS[i].addEventListener("click", () => {
            buildJob(response_json[i]);
        });
    }
}

const buildJobs = amount => {
    for (let i = 0; i < amount; i++) {
        const jobElement = document.createElement("div");
        jobElement.classList.add("job");
        jobElement.tabIndex = 0;

        const imageContainer = document.createElement("div");
        imageContainer.classList.add("image-container");
        jobElement.appendChild(imageContainer);

        const image = document.createElement("img");
        image.classList.add("image");
        if (response_json[i].company_logo == null) {
            image.src = "..\\..\\Images\\noImage.png"
        } else {
            image.src = response_json[i].company_logo;
        }
        imageContainer.appendChild(image);
    
        const information = document.createElement("div");
        information.classList.add("information-container");
        jobElement.appendChild(information);
    
        const title = document.createElement("h2");
        title.classList.add("title");
        title.innerText = response_json[i].title;
        information.appendChild(title);
    
        const company = document.createElement("h3");
        company.classList.add("company", "font-style");
        company.innerText = response_json[i].company;
        information.appendChild(company);
    
        const location = document.createElement("h3");
        location.classList.add("location", "font-style");
        location.innerText = response_json[i].location;
        information.appendChild(location);

        const descriptionContainer = document.createElement("div");
        descriptionContainer.classList.add("description-container");
        jobElement.appendChild(descriptionContainer);

        const description = document.createElement("p");
        description.classList.add("description");
        description.innerHTML = (""+response_json[i].description).substring(0, 150) + "...";
        descriptionContainer.appendChild(description);
    
        JOBS_CONTAINER.appendChild(jobElement);
    }

    JOBS = document.querySelectorAll(".job");
    jobListener();
}