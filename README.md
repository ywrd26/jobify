<h1>Jobify</h1>
Yousif Wardah<br>
Klausurersatzleistung WBS SoSe2020<br><br>

Auf Heroku gehostete App: <a href="https://vast-wave-44769.herokuapp.com/">Jobify</a>

<ol><h2>Contents</h2>
    <li>Utilized APIs</li>
    <li>Color coding</li>
    <li>Additional information</li>
    <li>Description</li>
    <li>Installation</li>
</ol>

<h2>Utilized APIs</h2>
<ol>Jobify mainly uses two APIs (Application Programming Interface):
<li>Being the core of Jobify, the first API  is <a href="https://jobs.github.com/api"><strong>Github Jobs API</strong></a>,  retrieving all the job postings that occur on the Github Jobs page.<br></li>
<li>The second applied API, the <a href="https://developer.adzuna.com/"><strong>adzuna API</strong></a>, recovers the estimated salary for a job section.</li>
</ol>

<h2>Color coding</h2>
<p> When desiging an application, the initial problem that arises is the selection of perfectly compatible colors. <br> Fortunately, Google provides a variety of color schemes, simplifying this process and narrowing it down to <a href="https://material.io/design/color/dark-theme.html">Google's Material IO Design (Dark theme)</a> for Jobify's mainly dark-themed application. </p>

<h2>Additional information</h2>
<p>Apart from the data prompted by the APIs, everything was written, designed and programmed by Yousif Wardah.<br>The cookie consent banner is provided by <a href="https://www.osano.com/">osano</a></p>

<h2>Description</h2>
<p>Jobify is an excellent application helping list all job openings for an easy & reliable way to discover which company is currently seeking employees, broadening applicants' possibilities during the hunt for a new, exciting and suitable workplace.<br> Providing not only an incredible specificity on entered criteria but also an estimated salary, Jobify also distinguishes itself by primarily catering to Software Engineers.  It is truly an application made <i>by</i> developers <i>for</i> developers.

<strong>How does it work?</strong>
<p>Jobify utilizes the GitHub Jobs API, which returns all job postings within a specified region in three compact steps, applying a flawlessly designed user interface that engages the user. The user is simultaneously cushioned by a foolproof simplicity. Thus, They are guaranteed smooth progression from an unsatisfying job to an enhanced level of comfort.<br>Regarding all standards considered for making a final choice on the listed offers, no settling for mediocrity is ever required. .<br>
The second step lies within the adzuna API that is used for displaying approximated income on the second page in a custom-built graph.</p>
<img src="screenshots/main.png">
<img src="screenshots/secondary.png">
<img src="screenshots/job.png">

<h2>Installation</h2>
This is a short guide for locally running the jobifiy application.
<ol> Please follow these steps precisely
    <li>
        <ol>Prerequisites: Software
            <li>
                <ul>Installing Git:
                    <li>Windows: <a href="https://git-scm.com/">https://git-scm.com/</a><br>Tutorial: <a href="https://www.youtube.com/watch?v=nbFwejIsHlY">https://www.youtube.com/watch?v=nbFwejIsHlY</a></li>
                <ul>
            </li>
            <li>
                <ul>Installing Node.js
                    <li>Windows: <a href="https://nodejs.org/en/">https://nodejs.org/en/</a><br>Tutorial: <a href="https://www.youtube.com/watch?v=JINE4D0Syqw">https://www.youtube.com/watch?v=JINE4D0Syqw</a></li>
                </ul>
            </li>
            <li>Node package manager (included within Node.js)</li>
        </ol>
    </li>
    <li>
        <ol>Further requirements: Accounts & API keys
            <li>Create an account & an app on <a href="https://developer.adzuna.com/">adzuna</a></li>
        </ol>
    </li>
    <li>
        <ol>If you have everything installed and you have created your adzuna account, then follow these instructions
            <li>Open your system console. The following command should be used within the console</li>
            <li>Create a folder for the application: mkdir "NAME_OF_FOLDER"</li>
            <li>Change your directory to that folder: cd "NAME_OF_FOLDER"</li>
            <li>Clone the project from Gitab: git clone https://git.thm.de/ywrd26/jobify.git</li>
            <li>Change the directory again: cd jobify</li>
            <li>Install the required dependencies: npm install</li>
            <li>Visit <a href="https://developer.adzuna.com/admin/access_details">https://developer.adzuna.com/admin/access_details</a> to view your Application ID and API key</li>
            <li>Replace the Api id with YOUR_API_ID_HERE in the .env file</li>
            <li>Replace the Api key with YOUR_API_KEY_HERE in the .env file</li>
            <li>Start the server: node server.js</li>
            <li>Finally open your browser with the URL displayed in the console</li>
        </ol>
    </li>
<ol>